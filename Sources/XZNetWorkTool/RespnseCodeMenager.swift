//
//  RespnseCodeMenager.swift
//  
//
//  Created by ZJZN CSCEC on 2021/11/30.
//

import Foundation

public class RespnseCodeMenager: NSObject {
    ///继承这个这个类，并且 重写这个函数 来处理 code
    class func custom_handCodeFunc(_ code: NSInteger, _ netData: Any?, _ error: Error?, _ url: URL?) {}
    ///继承这个这个类，并且 重写这个函数 来处理 成功code
    class func custom_handSucceedCodeFunc(_ netData: Any?, _ url: URL?) {}
    ///继承这个这个类，并且 重写这个函数 来处理 失败code
    class func custom_handDefeatCodeFunc(_ code: NSInteger,_ error: Error?, _ url: URL?) {}
    
    ///code log  处理
    class func handleCode (_ code: NSInteger, _ netData: Any?, _ error: Error?, _ url: URL?) -> (Bool) {
        custom_handCodeFunc(code, netData, error, url)
        if code / 100 == 2 {
            succeed(code,netData,url)
            custom_handSucceedCodeFunc(netData, url)
            return true
        }
        custom_handDefeatCodeFunc(code, error, url)
        defeat(code, error, url)
        return false
    }
}

/// log输出
private extension RespnseCodeMenager {
    
    class func succeed(_ code: NSInteger,_ netData: Any?, _ url: URL?) {
        if !isPrintSucceedNetWorkLog {
            return
        }
        
        let urlTemp: Any = url ?? "url 未知"
        let dataTemp: Any = netData ?? "data 未知"
        
        xzPrint("\n\n✅✅✅✅✅✅✅✅✅✅✅✅✅✅✅请求成功 code: \(code)\n✅\(urlTemp)\n")
        
        if let dataArray = (dataTemp as? Array<Any>) {
            xzPrint("etData(Array):--")
            for data in dataArray {
                xzPrint(data)
            }
        }else{
            xzPrint("\n\netData(Object):--")
            if let netDataObj = netData as? BaseModel {
               let netDataStr = netDataObj.toJSONString(prettyPrint: true)
                xzPrint("\n✅data：")
                xzPrint(netDataStr ?? "🌶没有数据")
                xzPrint("✅✅✅✅✅✅✅✅✅✅✅✅✅✅✅\n\n\n")
            }else{
                xzPrint(dataTemp)
                xzPrint("✅✅✅✅✅✅✅✅✅✅✅✅✅✅✅\n\n\n")
            }
        }
    }
    
    class func defeat(_ code: NSInteger,_ error: Error?, _ url: URL?) {
        
        if !isPrintErrorNetWorkLog {
            return
        }
        
        let urlTemp: Any = url ?? "url 未知"
        let errorTemp: Any = error ?? "error 未知"
        
        xzPrint("\n\n🌶🌶🌶🌶🌶🌶🌶🌶🌶🌶🌶🌶请求失败 code：\(code)\n\(urlTemp)\n")
        
        xzPrint("\n🌶error:--")
        xzPrint(errorTemp)
        xzPrint("🌶🌶🌶🌶🌶🌶🌶🌶🌶🌶🌶🌶\n\n\n\n")
    }
}

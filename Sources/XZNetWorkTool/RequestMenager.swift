//
//  RequestMenager.swift
//  RequestMenager
//
//  Created by ZJZN CSCEC on 2021/12/1.
//

import Foundation
import Alamofire


/// qury 参数
private let query_Parameter = URLEncoding.init(destination: .queryString)

/// body 参数
private let httpBody_Parameter = URLEncoding.init(destination: .httpBody)


class RequestMenager: NSObject {

    //MARK: - downLoad Request
    
    /// 获取 下载请求
    ///
    /// - Parameters:
    ///   - path: url
    ///   - HTTPMethod_: 请求方式
    ///   - parameters: 请求参数
    ///   - parametersType: 请求参数 拼接类型
    /// - Returns: DataRequest
    class func getRequest(Path path: String,HTTPMethod HTTPMethod_: HTTPMethod? = .get,_ parameters: [String:Any]? = nil,_ parametersType: ParamaetersType? = nil) ->(DataRequest?) {
        let request = RequestMenager.getLoadDataURLRequest(Path: path, HTTPMethod: HTTPMethod_, parameters, parametersType)
        if let request = request {

            return AlamofirManager.default.sessionManger.request(request)
        }
        return nil
    }
   
    class private func getLoadDataURLRequest(Path path: String,HTTPMethod HTTPMethod_: HTTPMethod? = .get,_ parameters: [String:Any]? = nil,_ parametersType: ParamaetersType? = nil) -> (URLRequest?){
      
        do{
            let url = try AlamofireURLMenager.getURL(path)
            var requst = URLRequest.init(url: url)
            requst.httpMethod = (HTTPMethod_ ?? .get).rawValue
            
            ///传入 一些全局header 比如
            for (value,key) in Alamofire_header ?? Dictionary() {
                requst.setValue(value, forHTTPHeaderField: key)
            }
            
            //传入版本  "Version": "2.1.0"
            
            switch parametersType ?? .query{
                
            case .query:
                return try query_Parameter.encode(requst, with: parameters)
                
            case .body:
                return try httpBody_Parameter.encode(requst, with: parameters)
            }
        } catch {
            xzPrint("🌶\n get请求 request 转化失败 " + path + "🌶\n")
            return nil
        }
    }
    
    //MARK: - upload Request
    class func UploadRequest(_ path : String,_ method: HTTPMethod,headers: HTTPHeaders? = nil) -> (URLRequest?){
        do {
            let url = try AlamofireURLMenager.getURL(path)
            var requst = try URLRequest(url: url, method: method, headers: headers)
            ///传入 一些全局header
            if let headers = Alamofire_header {
                for (value,key) in headers {
                    requst.setValue(value, forHTTPHeaderField: key)
                }
            }
            return requst
        }catch{
            xzPrint("🌶\n 数据上传 request 转化失败 " + path + "🌶\n")
            return nil
        }
    }
}

class AlamofireURLMenager: NSObject {
    
    private class func getBaseURLStr(_ str: String) -> (String) {
        return baseURL + str
    }
    ///返回一个url 并且 cach处理
    class func getURL(_ path:String) throws -> URL {
        var urlStr = AlamofireURLMenager.getBaseURLStr(path)
        urlStr = urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        guard let URL = URL(string: urlStr) else { throw AFError.invalidURL(url: urlStr) }
        return URL
    }
}

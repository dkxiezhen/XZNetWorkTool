//
//  AlamofirManager.swift
//  
//
//  Created by ZJZN CSCEC on 2021/11/30.
//

import Foundation
import Alamofire

///请求方式
enum RequestMethod: String {
    case options = "OPTIONS"
    case get = "GET"
    case head = "HEAD"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
    case trace = "TRACE"
    case connect = "CONNECT"
}

/// 参数拼接的类型
enum ParamaetersType: String {
    case body = "body"
    case query = "query"
}


/// 请求 数据类型
enum ResponseDateType: String {
    case array = "array"
    case object = "Object"
    case json = "json"
}

class AlamofirManager {
    
    static let `default`: AlamofirManager = AlamofirManager()
    
    var sessionManger: Session = {
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = Alamafire_TimeoutIntervalForRequest
        let manger = Alamofire.Session(configuration: config)
        return manger
    }()
}

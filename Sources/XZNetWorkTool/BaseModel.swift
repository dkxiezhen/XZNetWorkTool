//
//  File.swift
//  
//
//  Created by ZJZN CSCEC on 2021/11/30.
//

import Foundation
import ObjectMapper

//类模型
class BaseModel: NSObject, Mappable {
    ///已经key对应的属性将要赋值
    private var setingValueCallBack: ((_ key:String,_ value: AnyObject)->()?)?
    
    ///已经key对应的属性已经赋值
    private var setedValueCallBack: ((_ key:String,_ value: AnyObject)->()?)?
   
    ///已经key对应的属性将要赋值
    func setingValue(_ callBack: @escaping (_ key:String,_ value: AnyObject)->()?){
        setingValueCallBack = callBack
    }
    ///已经key对应的属性已经赋值
    func setedValue(_ callBack: @escaping (_ key:String,_ value: AnyObject)->()?){
        setedValueCallBack = callBack
    }

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        let propertyNames = self.getPropertyNames()
        for key in propertyNames {
            var property = value(forKey: key)
            property <- map[key]
            setingValueCallBack?(key,property as AnyObject)
            setValue(property, forKey: key)
            setedValueCallBack?(key,property as AnyObject)
        }
    }
}
extension NSObject {
    //获取属性列表名称
    func getPropertyNames() -> ([String]){
        var outCount:UInt32
        outCount = 0
        let propers = class_copyPropertyList(self.classForCoder, &outCount)!
        
        let count:Int = Int(outCount);
        var propertyArray = [String]()
        for i in 0...(count-1) {
            let aPro: objc_property_t = propers[i]
            let proName:String! = String.init(utf8String: property_getName(aPro))
            propertyArray.append(proName)
        }
        return propertyArray
    }
}

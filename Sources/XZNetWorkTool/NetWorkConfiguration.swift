//
//  File.swift
//  
//
//  Created by ZJZN CSCEC on 2021/11/30.
//

///一些统一的配置
import Foundation
import UIKit
///域名 配置
//let baseServerWord = "test"
//var baseServerWord = "demo"

/// release模式 是哪个环境,一定要写对，
let baseServerWord_release = "api"

///debug模式 下默认是什么环境
var baseServerWord_debug: String = "demo"

var baseURL: String {
    get {
        if !isDebug {
            return "http://\(baseServerWord_release).dianping.com/"
        }
        return "http://\(baseServerWord_debug).dianping.com/"
    }
}


//MARK: - code 的处理

///code 处理 是否打印Log日志
let isPrintSucceedNetWorkLog: Bool = true
///是否打印失败请求
let isPrintErrorNetWorkLog: Bool = true
///是否打印请求成功后的数据
let isPrintSucceedData: Bool = true

//MARK: - 超时时间
///超时时间
let Alamafire_TimeoutIntervalForRequest:TimeInterval = 10


//MARK: - 所有请求都会带的东西比如 版本和 cookie
var Alamofire_header: [String:String]? {
    get {
        return [
//            "Cookie" : KRUserInfoManager.shared.cookieManager.clientCookie,
            "Version": XZ_Version
        ]
    }
}

private var versionPrivate: String?
var XZ_Version: String {
    get {
        if let versionPrivate_ = versionPrivate {
            return versionPrivate_
        }
        versionPrivate = (Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String)
        return versionPrivate ?? "没有version😁"
    }
}


/**
 * log 在release 版本不打印
 * 注意要在 项目的 budSeting中 查找 `Other Swift Flags`，修改debug模式的flag 为“DEBUG”
 */
func xzPrint(_ item: @autoclosure () -> Any) {
    if isDebug || isPrintSucceedData{
        print(item())
    }
}

///是否为debug模式
var isDebug: Bool {
    get {
        #if DEBUG
            return true
        #else
            return false
        #endif
    }
}
class AlamofireConfiguration_Private: NSObject {

}
